require('dotenv').config(); // para añadir el fichero .env donde estan las variables de entorno

const express = require('express');
const app = express(); // inicializar framework de express
app.use(express.json()); // cambia el formtao de la variable a json para que cuando llega un objeto lo parsee a json

const port = process.env.PORT || 3000; // variable de entorno port o valor 3000 --> nosotros no tenemos definida la variable PORT
// en process.env estan almacenadas las variables de entorno, bien las que se definene en el sistema operativo bien las que hay definidas en el fichero .env

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const acountController = require('./controllers/AcountController');
// el directorio es ./ porque por defeto busca en node_modules, tengo que indicar un dir mas arriba

//funcion para que funcionen las llamadas desde polymer
var enableCORS =function(req,res,next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");// para añadir mas es esta linea y el segundo parametro es el nombre dle atributo que mandamos

  next();
}
app.use(enableCORS);

app.listen(port); //inicializa el servidor en el puerto indicado
console.log("API escuchando en el puerto BIP BIP BIP "+ port);



app.get("/apitechu/v1/hello",
  function (req, res){
    console.log("GET /apitechu/v1/hello");

    //res.send('{"msg":"Hola desde APITechU"}'); // envia un string que se puede parsear como un json
    res.send({"msg":"Hola desde APITechU"}); // envia un json
  }
)


app.get("/apitechu/v1_0/users",
  function (req, res){
    console.log("GET /apitechu/v1/users");
    // res.sendFile('usuarios.json',{root:__dirname});
    var users = require('./usuarios.json');
    res.send(users);

  }
)



app.get("/apitechu/v1/users",userController.getUsersV1); //pasamos la funcion, no la llamamos, por eso no le ponemos los parametros

app.get("/apitechu/v2/users",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUsersByIdV2);

app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v1_1/users",userController.createUserV1_1);


app.post("/apitechu/v2/users", userController.createUserV2);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);
app.delete("/apitechu/v2/users/:id",userController.deleteUserV2);

app.post("/apitechu/v1/monstruo/:v1/:v2",  // con /: indico los parametros de entrada, obligatorios en la url, el parametro es obligatorio
  function (req, res){
    console.log("POST /apitechu/v1/monstruo/:v1/:v2");
    console.log("----------------------");
    console.log("Parametros");
    console.log(req.params);

    console.log("----------------------");
    console.log("Query String");
    console.log(req.query);

    console.log("----------------------");
    console.log("Headers");
    console.log(req.headers);

    console.log("----------------------");
    console.log("Body");
    console.log(req.body);

  }//function
)// post



app.post("/apitechu/v1/login",authController.loginV1);
app.post("/apitechu/v2/login",authController.loginV2);
app.post("/apitechu/v1/logout/:id",authController.logoutV1);
app.post("/apitechu/v2/logout/:id",authController.logoutV2);


app.get("/apitechu/v1/accounts",acountController.getAccountsV1);
app.get("/apitechu/v1/accounts/:id",acountController.getAccountByIdV2); 
