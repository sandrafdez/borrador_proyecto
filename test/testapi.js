const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should(); //metodo para hacer la asercion, es decir, pasar el test unitario

describe("First test",
  function (){
    it("Test that DuckDickGO works", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      chai.request('http://www.duckduckgo.com') // se define dominio base donde hacer las peticiones
      .get('/')  // se hace un get sin añadir más, sin añadir patch
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("Request finished");
          //console.log(res);
          //console.log(err);
          res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
          done(); // indica al framework de aserciones cuando puede comprobar las aserciones
        }//function (err,res)

      )//end
    }//funciton (done)
    )//it
  }//function


)//describe


describe("Test de API Usuarios",
  function (){
    it("Prueba que la  API responde", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
      chai.request('http://localhost:3000') // se define dominio base donde hacer las peticiones
      .get('/apitechu/v1/hello')  // se añade el path
      .end(   //con la respuesta ve a este función
        function(err, res){
          console.log("Request finished");
          //console.log(res);
          //console.log(err);
          res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
          res.body.msg.should.be.eql("Hola desde APITechU");
          done(); // indica al framework de aserciones cuando puede comprobar las aserciones
        }//function (err,res)

      )//end
    }//funciton (done)
    ),//it Prueba que la  API responde
    it("Prueba que la  API devuelve lista de usuarios correctos", function (done){  //cada it es un test unitario con una funciona manejadora donde se hace el test unitario en si
        chai.request('http://localhost:3000') // se define dominio base donde hacer las peticiones
        .get('/apitechu/v1/users')  // se añade el path
        .end(   //con la respuesta ve a este función
          function(err, res){
            console.log("Request finished");
            //console.log(res);
            //console.log(err);
            res.should.have.status(200);  //200 es el codigo que devuelve cuando ha ido ok
            //res.body.msg.should.be.eql("Hola desde APITechU");
            res.body.usuarios.should.be.a("array");

            for (user of res.body.usuarios)
            {
              user.should.have.property("first_name"); // comprobamos la existencia de la propiedad no el valor
              user.should.have.property("email");
            }//for

            done(); // indica al framework de aserciones cuando puede comprobar las aserciones
          }//function (err,res)

        )//end
      }//funciton (done)

    )// it Prueba que la  API devuelve lista de usuarios correctos
  }//function


)//describe
