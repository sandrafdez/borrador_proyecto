const requestJson = require ('request-json');
const io = require ('../io'); // para usar las funciones que me he llevado a otro fichero
const crypt = require('../crypt');


const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js


function getAccountsV1 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v1/accounts");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("accounts?" + mLabAPIKey,
    function (err,resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }//function (err,resMLab, body)

  );//  httpClient.get

  console.log("FIN GET  /apitechu/v1/accounts");
  console.log("***********************");

}//getAccountsV1



function getAccountByIdV2 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v1/accounts/:id");
  var id = req.params.id;
  var query = "q={'userId':"+id+"}";

console.log("La consulta es "+query);
console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("accounts?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
        var response ={ "msg" : "Error obteniendo cuentas"}
        res.status(500);
      } else {
        if (body.length >0){
          var response = body;
        } else {
          var response = {"msg" : "cuentas no encontrado"}
          res.status(404);
        }

      }

      res.send(response);
    }//function (err,resMLab, body)

  );//  httpClient.get

  console.log("FIN GET  /apitechu/v1/accounts/:id)");
  console.log("***********************");

}//getAccountByIdV2


module.exports.getAccountsV1 = getAccountsV1;
module.exports.getAccountByIdV2 = getAccountByIdV2;
