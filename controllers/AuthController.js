const requestJson = require ('request-json');
const io = require ('../io'); // para usar las funciones que me he llevado a otro fichero
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js


function loginV1(req, res){
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');

  var login = false;
  var idLogin;
  var mailOK;
  var user;
  for (var i = 0; i < users.length; i++)
  {
    console.log(" i "+i);
    user = users[i];
    console.log("id "+ user.id);
    if (user.email == req.body.email )
    {
      console.log("usuario encontrado");
      mailOK=true;
      if (user.password==req.body.password){
        console.log("pass encontrado");
        user.logged=true;
        console.log("  user.logged "+  user.logged);
        login = true;
        idLogin = user.id;
        break;
      }
    }

  }


  if (login) {
    io.writeUserDataToFile(users);
    console.log("Proceso login usuario terminado OK");
    res.send({"msg" : "usuario logado" ,
  "idUsuario": idLogin}); // si idLogin es undefine no escribe esta propiedad en la salida
} else if (mailOK){
    console.log("Proceso login usuario terminado usuarioa encontrado pero  pass no encontrada");
    res.send({"msg" : "pass no encontrado" });
  } else {
    console.log("Proceso login usuario terminado usuario no encontrado ");
    res.send({"msg" : "usuario no encontrado" });
  }

}//loginV1



function logoutV1(req, res){
  console.log("POST /apitechu/v1/logoutV1");

  console.log("La id del usuario a logout es "+req.params.id);



  var users = require('../usuarios.json');

  var logout  = false;
  var idLogin;

  var user;
  for (var i = 0; i < users.length; i++)
  {
    console.log(" i "+i);
    user = users[i];
    console.log("id "+ user.id);
    if (user.id == req.params.id )
    {
      console.log("usuario encontrado");
      if (user.logged === true){  // triple = compara valor y tipo de objeto
        console.log("usuario logado ");
        delete user.logged;
        logout  = true;
        idLogin = user.id;
      }
      break;

    }

  }//for

console.log("logout "+logout);
  if (logout ) {
    io.writeUserDataToFile(users);
    console.log("Proceso logout  usuario terminado OK");
    res.send({"msg" : "logout correcto" ,
                "idUsuario": idLogin});
  } else {
    console.log("Proceso logout usuario incorrecto ");
    res.send({"msg" : "logout incorrecto" });
  }

}//logoutV1



function loginV2(req,res)
{
  console.log("POST /apitechu/v2/login");

  console.log(req.body.email);
  console.log(req.body.password);
  var email =req.body.email;
  var passIntroducida =req.body.password;
  var query = "q={'email':'"+email+"'}";
  var response ;
  console.log("La consulta es "+query);
  console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("users?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
         response ={ "msg" : "Error obteniendo usuario"}
        res.status(500).send(response);
      } else {
        if (body.length >0 && crypt.checkPassword(passIntroducida,body[0].password) ){
          console.log("id "+body[0].id);
          console.log("email "+body[0].email);
          console.log("password "+body[0].password);

            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("users?"+ query +"&"+ mLabAPIKey,JSON.parse(putBody),
              function (errPUT,resMLabPUT, bodyPUT){
                  console.log("body");
                  console.log(bodyPUT);
                  (bodyPUT.n == 1) ? res.status(200).send({"msg" : "Usuario "+email +" logado correctamente","id": body[0].id }) : res.status(500).send({"msg" : "error al logar al usuario "+email });
                }//function (err,resMLab, body)
              )//httpClient.put(

        } else {
          response = {"msg" : "usuario "+ email +" o contaseña no encontrados"}
          res.status(404).send(response);
        }
      }
    }//function (err,resMLab, body)
  );//  httpClient.get
}//function loginV2(req,res)


function logoutV2(req,res)
{
  console.log("POST /apitechu/v2/logout");
  console.log("La id del usuario a logout es "+req.params.id);
  var id =req.params.id;
  var query = "q={'id':"+id+"}";
  var response ;
  console.log("La consulta es "+query);
  console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("users?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
         response ={ "msg" : "Error obteniendo usuarios"}
        res.status(500).send(response);
      } else {
        if (body.length >0){
          console.log("id "+body[0].id);
          console.log("email "+body[0].email);
          console.log("logged "+body[0].logged);
          if (body[0].logged === true)
          {
            var putBody = '{"$unset":{"logged":""}}'
            httpClient.put("users?"+ query +"&"+ mLabAPIKey,JSON.parse(putBody),
              function (errPUT,resMLabPUT, bodyPUT){
                  console.log("body");
                  console.log(bodyPUT);
                  (bodyPUT.n == 1) ? res.status(200).send({"msg" : "Usuario "+id +" deslogado correctamente" }) : res.status(500).send({"msg" : "error al actualizar login del id "+id });
                }//function (err,resMLab, body)
              )//httpClient.put(
          }else {
              response = { "msg" : "usario "+id +" no logado"};
              res.status(404).send(response);
          }
        } else {
           response = {"msg" : "usuarios no encontrado"}
           res.status(404).send(response);
        }
      }
    }//function (err,resMLab, body)
  );//  httpClient.get
}//function logoutV2(req,res)



module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
module.exports.loginV2 = loginV2;
