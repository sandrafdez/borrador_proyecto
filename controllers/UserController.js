const requestJson = require ('request-json');
const io = require ('../io'); // para usar las funciones que me he llevado a otro fichero
const crypt = require('../crypt');


const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusfb11ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // usamos la variable de entorno definida en el fichero .env, esto está indicado en el server.js

function getUsersV1 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v1/users");
  console.log(req.query);

  console.log(" $top "+req.query.$top);
  console.log (" $count "+req.query.$count);
  var top = req.query.$top;
  var count = req.query.$count;

  var users = require('../usuarios.json');
  var topUser;
  if (top)
  {
    topUser = users.slice(0,top);
  } else {
    topUser = users;
  }

  var obj;
  if (count == "true")
  {
    var countUser = users.length;
    console.log (" countUSer "+countUser);
    obj = { "usuarios": topUser,
            "count":countUser  };
  } else {
    console.log (" countUSer sin ")
    obj = {"usuarios": topUser};
  }
  res.send(obj);
}//getV1


function getUsersV2 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("users?" + mLabAPIKey,
    function (err,resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }//function (err,resMLab, body)

  );//  httpClient.get

  console.log("FIN GET  /apitechu/v2/users");
  console.log("***********************");

}//getUsersV2

function getUsersByIdV2 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = "q={'id':"+id+"}";

console.log("La consulta es "+query);
console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("users?"+ query +"&"+ mLabAPIKey,
    function (err,resMLab, body){
      if (err){
        var response ={ "msg" : "Error obteniendo usuarios"}
        res.status(500);
      } else {
        if (body.length >0){
          var response = body[0];
        } else {
          var response = {"msg" : "usuarios no encontrado"}
          res.status(404);
        }

      }

      res.send(response);
    }//function (err,resMLab, body)

  );//  httpClient.get

  console.log("FIN GET  /apitechu/v2/users/:id)");
  console.log("***********************");

}//getUsersByIdV2

function getUsersByIdV21 (req, res){
  console.log("----------------------");
  console.log("GET  /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = "q={'id':"+id+"}";

console.log("La consulta es "+query);
console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("users?"+ query +"&"
   + mLabAPIKey,
    function (err,resMLab, body){
      console.log("resMLab ");
      console.log(resMLab);
      console.log("body ");
      console.log(body);
      var response = !err ? body[0] : {   // el body es un array de un elemento, por lo que es mejor que devolvamos directamente el elemento
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }//function (err,resMLab, body)

  );//  httpClient.get

  console.log("FIN GET  /apitechu/v2/users/:id)");
  console.log("***********************");

}//getUsersByIdV21

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");

  console.log(" id es "+req.body.id); // ojo con el tipo si se guarda como string o entero
  console.log(" first_name es "+req.body.first_name);
  console.log(" last_name es "+req.body.last_name);
  console.log(" email es "+req.body.email);
  console.log(" pass es "+req.body.password);

  var newUser = {
    "id" :req.body.id,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":crypt.hash(req.body.password)
  }
  console.log(newUser); // si concatenamos un string pondra object en la traza

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("users?" + mLabAPIKey, newUser,  //ojo con el nombre de la coleccion
  function(err,resMLab,body){
    console.log("Useuario creado");
    res.status(201).send({"msg":"Usuario creado"}); // 201 codigo para creado
  }//function(err,resMLab,body)

)//httpClient.post


}//function createUserV2

function createUserV1_1(req, res){
  console.log("POST /apitechu/v1_1/users");

  console.log(req.headers);

  console.log(req.headers.first_name);
  console.log(req.headers.last_name);
  console.log(req.headers.email);

  var newUser = {
    "first_name" :req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email
  };
  var users = require('../usuarios.json');
  users.push(newUser); //añadir un elemento al final del array
  console.log("Usuario añdido al array");

  //escribir en fichero
  io.writeUserDataToFile(users);
  console.log("Proceso creación usuario terminado");

  res.send({"msg" : "usuario creado"});

}//createUser


function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");

  console.log(req.headers);

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" :req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };
  var users = require('../usuarios.json');
  users.push(newUser); //añadir un elemento al final del array
  console.log("Usuario añdido al array");

  //escribir en fichero
  io.writeUserDataToFile(users);
  console.log("Proceso creación usuario terminado");

  res.send({"msg" : "usuario creado"});

}//createUserV1_1




/**PRACTICA BORRADO DE USUARIO*
Alternativas:

1 for normal
// for (var i = 0; i < users.length; i++)

2 Array forEach Ejecuta una función en cada elemento.

3 for (element in object) Itera en las propiedades enumerables de un objeto. ¡OJO! element no es lo que esperas.

4 for (element of iterable) Recorre cada elemento de un objeto que sea iterable (Array, Map).

5 Array findIndex

Asumimos que las ids son únicas, no se repiten en el array.

Recordar que los bucles se pueden parar usando break (¿se pueden parar todos?).

Array.splice es útil.

Orden sugerido 1, 3, 2, 4, 5
*/
function deleteUserV1 (req, res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log(req.params);

  console.log("La id del usuario a borrar es "+req.params.id);
  var idParam = req.params.id;
  var users = require('../usuarios.json');

  /*
  1 for normal

  var user;
  for (var i = 0; i < users.length; i++)
  {
    console.log(" i "+i);
    user = users[i];
    console.log("id "+ user.id);
    if (user.id == idParam)
    {
      console.log("dentro de if");
      users.splice(i,1);
      break;
    }

  }
  */


  // 3 for (element in object) Itera en las propiedades enumerables de un objeto. ¡OJO! element no es lo que esperas.
  /*
  for (i in users) {
    console.log("i "+ i);
    user = users[i];
    console.log("id "+ user.id);
    if (user.id == idParam)
    {
      console.log("dentro de if");
      users.splice(i,1);
      break;
    }
  }//for in
  */

  // 2 Array forEach Ejecuta una función en cada elemento.
  /*
  function deleteElements(user, i, users) {

    console.log('a[' + i + '] = ' + user);
    console.log("i "+ i);
    //user = users[i];
    console.log("id "+ user.id);
    if (user.id == idParam)
    {
      console.log("dentro de if");
      users.splice(i,1);
    }
  }//function deleteElements

  users.forEach(deleteElements);
  */

  // 4 for (element of iterable) Recorre cada elemento de un objeto que sea iterable (Array, Map).
  /*
  var i=0;
  for (user of users) {
    console.log("i "+i);
    console.log("user "+ user);
    console.log("id "+ user.id);
    if (user.id == idParam)
    {
      console.log("dentro de if");
      users.splice(i,1);
      break;
    }
    i++;
  }// for + i
  */

  // destrucctin, nodeJS v6+
  for (var[i,user] of users.entries()) {
    console.log("i "+i);
    console.log("user "+ user);
    console.log("id "+ user.id);
    if (user.id == idParam)
    {
      console.log("dentro de if");
      users.splice(i,1);
      break;
    }
  }// for of entries


  // 5 Array findIndex
  /*
  function isUserId(user) {
    console.log("dentro de isUserId "+ user.id);
    return user.id == idParam;
  }
  var i = users.findIndex(isUserId);
  if(i >= 0)
  {
    users.splice(i,1);
  }
  */

  console.log("Usuario borrado del array");

  //escribir en fichero
  io.writeUserDataToFile(users);
  console.log("Proceso eliminación usuario terminado");

  res.send({"msg" : "usuario borrado"});
}//deleteUserV1


function deleteUserV2(req, res){
  console.log("DELETE /apitechu/v2/users/:id");

  console.log(" id es "+req.params.id); // ojo con el tipo si se guarda como string o entero


  var id = req.params.id;
  var query = "q={'id':"+id+"}";

  console.log("La consulta es "+query);
  console.log("url completa "+baseMLabURL+"users?"+ query +"&" + mLabAPIKey)

  var httpClient = requestJson.createClient(baseMLabURL);

  var deleteUser  = [];

  httpClient.put("users?"+ query +"&"
   + mLabAPIKey,deleteUser,
    function (err,resMLab, body){
      console.log("resMLab");
      console.log(resMLab);
      console.log("body");
        console.log(body);
      var response = (body.removed == 0) ? res.status(401).send({"msg" : "Usuario no encontrado" }) : res.status(200).send({"msg" : "Usuario "+id+" eliminado correctamente" })


    }//function (err,resMLab, body)

  );//  httpClient.put

  console.log("FIN DELETE  /apitechu/v2/users/:id)");
  console.log("***********************");


}//function deleteUserV2(req, res)

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV1_1 = createUserV1_1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1  = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
module.exports.getUsersByIdV2 =getUsersByIdV2;
