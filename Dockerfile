# Dockerfile

#Imagen raiz
FROM node

#directorio de trabajo (Carpeta raiz)
WORKDIR /apitechu

#Copia de archivo de local a imagen
ADD . /apitechu

#Instalación de las dependencias
#comando RUN se ejecuta para generar la imagen
#comando CMD se ejecuta en la imagen una vez creada

RUN npm install --only=prod
# no añade las dependencias que esten en devDependencies

#puerto que vamos a usar (puerto del contendor)
EXPOSE 3000

#comando de inicializacion
CMD ["node","server.js"]
