const bcrypt = require('bcrypt');

function hash(data){
  console.log("Hashing data");
  return bcrypt.hashSync(data,10); // pasa 10 el algoritmo / esta funcion tiene el homologo asincrono que nos ahorra tiempo
}//function hash(data)

function checkPassword(passwordFromUserInPlainText,passwordFromDBHashed)
{
  console.log("Checking password");
  return bcrypt.compareSync(passwordFromUserInPlainText,passwordFromDBHashed);// devuelve boolean
}


module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
